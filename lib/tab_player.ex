defmodule TabPlayer do
    def parse(tab) do
      String.split(tab, "\n")
      |> Enum.filter(fn e -> e != ""  end)
      |> Enum.map(fn line -> parse_line(line) end)
      |> Enum.filter(fn e -> e != []  end)
      |> Enum.zip
      |> Enum.map(fn t -> Tuple.to_list(t) end)
      |> List.flatten
      |> Enum.join(" ")
    end

    def parse_line(line) do
        letter = String.first(line)
        Regex.scan(~r/\d+/, line)
        |> Enum.map(fn [n] -> letter <> n end)
    end
end